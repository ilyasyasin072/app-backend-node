var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const bodyParser = require('body-parser');


var indexRouter = require('./routes/Dashboard/index');
var usersRouter = require('./routes/users');
var authorRouter = require('./routes/Authors/authors');
var employeeRouter = require('./routes/Employees/employee');
var productRouter = require('./routes/Products/product');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// body parse
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json({
  limit: "8mb",
}));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/authors', authorRouter);
app.use('/employees', employeeRouter);
app.use('/products', productRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
