/*jslint
  browser: true, indent: 2, vars: true, unparam: true, sub: true */
/*global
  jQuery, TableTools, ace, alert */

  jQuery(function ($) {
    'use strict';
  
    // ace settings
    try {
      ace.settings.check('navbar', 'fixed');
      ace.settings.check('main-container', 'fixed');
      ace.settings.check('sidebar', 'collapsed');
      ace.settings.check('breadcrumbs', 'fixed');
    } catch (ignore) {}
  
    //tooltip placement on right or left
    function tooltip_placement(context, source) {
      var $source = $(source);
      var $parent = $source.closest('table');
      var off1 = $parent.offset();
      var w1 = $parent.width();
  
      var off2 = $source.offset();
      //var w2 = $source.width();
  
      if (parseInt(off2.left, 10) < parseInt(off1.left, 10) + parseInt(w1 / 2, 10)) {
        return 'right';
      }
      return 'left';
    }
  
    // From table page
    if ($('.datatable-default').length > 0) {
      //initiate dataTables plugin
      var oTable1, tableTools_obj;
      oTable1 = $('.datatable-default').dataTable({
        columnDefs: [{
          targets: ['nosearch'],
          searchable: false
        }, {
          targets: ['nosort'],
          orderable: false
        }],
        order: [],
        pageLength: 50
      });
  
      //TableTools settings
      TableTools.classes.container = "btn-group btn-overlap";
      TableTools.classes.print = {
        body: "DTTT_Print",
        info: "tableTools-alert gritter-item-wrapper gritter-info gritter-center white",
        message: "tableTools-print-navbar"
      };
  
      //initiate TableTools extension
      tableTools_obj = new $.fn.dataTable.TableTools(oTable1, {
        sSwfPath: "/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf", //in Ace demo ../assets will be replaced by correct assets path
  
        sRowSelector: "td:not(:last-child)",
        sRowSelect: "multi",
        fnRowSelected: function (row) {
          //check checkbox when row is selected
          try {
            $(row).find('input[type=checkbox]').get(0).checked = true;
          } catch (ignore) {}
        },
        fnRowDeselected: function (row) {
          //uncheck checkbox
          try {
            $(row).find('input[type=checkbox]').get(0).checked = false;
          } catch (ignore) {}
        },
  
        sSelectedClass: "success",
        aButtons: [
          {
            sExtends: "copy",
            sToolTip: "Copy to clipboard",
            sButtonClass: "btn btn-white btn-primary btn-bold",
            sButtonText: "<i class='fa fa-copy bigger-110 pink'></i>",
            fnComplete: function () {
              this.fnInfo(
                '<h3 class="no-margin-top smaller">Table copied</h3>' +
                        '<p>Copied ' + (oTable1.fnSettings().fnRecordsTotal()) + ' row(s) to the clipboard.</p>',
                1500
              );
            }
          },
  
          {
            sExtends: "csv",
            sToolTip: "Export to CSV",
            sButtonClass: "btn btn-white btn-primary  btn-bold",
            sButtonText: "<i class='fa fa-file-excel-o bigger-110 green'></i>"
          },
  
          {
            sExtends: "pdf",
            sToolTip: "Export to PDF",
            sButtonClass: "btn btn-white btn-primary  btn-bold",
            sButtonText: "<i class='fa fa-file-pdf-o bigger-110 red'></i>"
          },
  
          {
            sExtends: "print",
            sToolTip: "Print view",
            sButtonClass: "btn btn-white btn-primary  btn-bold",
            sButtonText: "<i class='fa fa-print bigger-110 grey'></i>",
  
            sMessage: "<div class='navbar navbar-default'><div class='navbar-header pull-left'><a class='navbar-brand' href='#'><small>" + $('.table-header:first').text() + "</small></a></div></div>",
  
            sInfo: "<h3 class='no-margin-top'>Print view</h3>" +
                    "<p>Please use your browser's print function to" +
                    "print this table." +
                    "<br />Press <b>escape</b> when finished.</p>"
          }
        ]
      });
      //we put a container before our table and append TableTools element to it
      $(tableTools_obj.fnContainer()).appendTo($('.tableTools-container'));
  
      //also add tooltips to table tools buttons
      //addding tooltips directly to "A" buttons results in buttons disappearing (weired! don't know why!)
      //so we add tooltips to the "DIV" child after it becomes inserted
      //flash objects inside table tools buttons are inserted with some delay (100ms) (for some reason)
      setTimeout(function () {
        $(tableTools_obj.fnContainer()).find('a.DTTT_button').each(function () {
          var div = $(this).find('> div');
          if (div.length > 0) {
            div.tooltip({
              container: 'body'
            });
          } else {
            $(this).tooltip({
              container: 'body'
            });
          }
        });
      }, 200);
  
      /////////////////////////////////
      //table checkboxes
      $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);
  
      //select/deselect all rows according to table header checkbox
      $('.datatable-default > thead > tr > th input[type=checkbox]').eq(0).on('click', function () {
        var th_checked = this.checked; //checkbox inside "TH" table header
  
        $(this).closest('table').find('tbody > tr').each(function () {
          var row = this;
          if (th_checked) {
            tableTools_obj.fnSelect(row);
          } else {
            tableTools_obj.fnDeselect(row);
          }
        });
      });
  
      //select/deselect a row when the checkbox is checked/unchecked
      $('.datatable-default').on('click', 'td input[type=checkbox]', function () {
        var row = $(this).closest('tr').get(0);
        if (!this.checked) {
          tableTools_obj.fnSelect(row);
        } else {
          tableTools_obj.fnDeselect($(this).closest('tr').get(0));
        }
      });
  
      $(document).on('click', '.datatable-default .dropdown-toggle', function (e) {
        e.stopImmediatePropagation();
        e.stopPropagation();
        e.preventDefault();
      });
  
      /********************************/
      //add tooltip for small view action buttons in dropdown menu
      $('[data-rel="tooltip"]').tooltip({
        placement: tooltip_placement
      });
    }
  
    // From form-elements page
  
    //select2
    $.fn.select2.defaults.set("theme", "bootstrap");
    $.fn.select2.defaults.set("width", "100%");
    $('.select2').select2({
      allowClear: true
    }).on('select2:select', function (evt) {
      var self = $(evt.target);
      var form = self.parents('form:eq(0)');
      var focusable = form.find('input,a,select,button,textarea,div[contenteditable=true]').filter(':visible');
  
      // Focus on the next field
      focusable.eq(focusable.index(self) + 1).focus();
    });
  
    $('.select2-ajax-data').select2({
      ajax: {
        dataType: 'json',
        delay: 250,
        data: function (params) {
          var query = {
            q: params.term, // search term
            page: params.page
          };
          var dependTo = $(this.context).data("dependTo");
          if (dependTo) {
            query["depended_id"] = $(dependTo).val();
          }
          return query;
        },
        processResults: function (data, params) {
          // parse the results into the format expected by Select2
          // since we are using custom formatting functions we do not need to
          // alter the remote JSON data, except to indicate that infinite
          // scrolling can be used
          params.page = params.page || 1;
  
          return {
            results: data.items,
            pagination: {
              more: (params.page * 30) < data.total_count
            }
          };
        },
        cache: true
      },
      escapeMarkup: function (markup) { return markup; },
      minimumInputLength: 3,
      templateResult: function (data) {
        return data.text;
      },
      templateSelection: function (data) {
        return data.text;
      },
      allowClear: true,
      placeholder: $(this).data('placeholder')
    }).on('select2:select', function (evt) {
      var self = $(evt.target);
      var form = self.parents('form:eq(0)');
      var focusable = form.find('input,a,select,button,textarea,div[contenteditable=true]').filter(':visible');
  
      // Focus on the next field
      focusable.eq(focusable.index(self) + 1).focus();
    });
  
    $('[data-rel=tooltip]').tooltip({
      container: 'body'
    });
    $('[data-rel=popover]').popover({
      container: 'body'
    });
  
    $('textarea[class*=autosize]').autosize({
      append: "\n"
    });
    $('textarea.limited').inputlimiter({
      remText: '%n character%s remaining...',
      limitText: 'max allowed : %n.'
    });
  
    $.mask.definitions['~'] = '[+-]';
    $('.input-mask-date').mask('99/99/9999');
    $('.input-mask-phone').mask('(999) 999-9999');
    $('.input-mask-eyescript').mask('~9.99 ~9.99 999');
    $(".input-mask-product").mask("a*-999-a999", {
      placeholder: " ",
      completed: function () {
        alert("You typed the following: " + this.val());
      }
    });
  
    $('.input-file-1').ace_file_input({
      no_file: 'No File ...',
      btn_choose: 'Choose',
      btn_change: 'Change',
      droppable: false,
      onchange: null,
      thumbnail: false //| true | large
      //whitelist:'gif|png|jpg|jpeg'
      //blacklist:'exe|php'
      //onchange:''
      //
    });
    //pre-show a file name, for example a previously selected file
    //$('.input-file-1').ace_file_input('show_file_list', ['myfile.txt'])
  
    $('.input-file-3').ace_file_input({
      style: 'well',
      btn_choose: 'Drop files here or click to choose',
      btn_change: null,
      no_icon: 'ace-icon fa fa-cloud-upload',
      droppable: true,
      thumbnail: 'small'//, //large | fit
      //,icon_remove:null//set null, to hide remove/reset button
      /**,before_change:function(files, dropped) {
        //Check an example below
        //or examples/file-upload.html
        return true;
      }*/
      /**,before_remove : function () {
        return true;
      }*/
      // preview_error: function (filename, error_code) {
        //name of the file that failed
        //error_code values
        //1 = 'FILE_LOAD_FAILED',
        //2 = 'IMAGE_LOAD_FAILED',
        //3 = 'THUMBNAIL_FAILED'
        //alert(error_code);
      // }
    // }).on('change', function () {
      //console.log($(this).data('ace_input_files'));
      //console.log($(this).data('ace_input_method'));
    });
  
    //$('.input-file-3')
    //.ace_file_input('show_file_list', [
    //{type: 'image', name: 'name of image', path: 'http://path/to/image/for/preview'},
    //{type: 'file', name: 'hello.txt'}
    //]);
  
    //dynamically change allowed formats by changing allowExt && allowMime function
    $('#id-file-format').removeAttr('checked').on('change', function () {
      var whitelist_ext, whitelist_mime;
      var btn_choose;
      var no_icon;
      if (this.checked) {
        btn_choose = "Drop images here or click to choose";
        no_icon = "ace-icon fa fa-picture-o";
  
        whitelist_ext = ["jpeg", "jpg", "png", "gif", "bmp"];
        whitelist_mime = ["image/jpg", "image/jpeg", "image/png", "image/gif", "image/bmp"];
      } else {
        btn_choose = "Drop files here or click to choose";
        no_icon = "ace-icon fa fa-cloud-upload";
  
        whitelist_ext = null; //all extensions are acceptable
        whitelist_mime = null; //all mimes are acceptable
      }
      var file_input = $('.input-file-3');
      file_input
        .ace_file_input('update_settings', {
          btn_choose: btn_choose,
          no_icon: no_icon,
          allowExt: whitelist_ext,
          allowMime: whitelist_mime
        });
      file_input.ace_file_input('reset_input');
  
      file_input
        .off('file.error.ace');
        // .on('file.error.ace', function (e, info) {
          //console.log(info.file_count);//number of selected files
          //console.log(info.invalid_count);//number of invalid files
          //console.log(info.error_list);//a list of errors in the following format
  
          //info.error_count['ext']
          //info.error_count['mime']
          //info.error_count['size']
  
          //info.error_list['ext']  = [list of file names with invalid extension]
          //info.error_list['mime'] = [list of file names with invalid mimetype]
          //info.error_list['size'] = [list of file names with invalid size]
  
          /**
          if ( !info.dropped ) {
            //perhapse reset file field if files have been selected, and there are invalid files among them
            //when files are dropped, only valid files will be added to our file array
            e.preventDefault();//it will rest input
          }
          */
  
          //if files have been selected (not dropped), you can choose to reset input
          //because browser keeps all selected files anyway and this cannot be changed
          //we can only reset file field to become empty again
          //on any case you still should check files with your server side script
          //because any arbitrary file can be uploaded by user and it's not safe to rely on browser-side measures
        // });
    });
  
    $('.spinner1')
      .ace_spinner({
        value: 0,
        min: 0,
        max: 200,
        step: 10,
        btn_up_class: 'btn-info',
        btn_down_class: 'btn-info'
      })
      .closest('.ace-spinner')
      .on('changed.fu.spinbox', function () {
        console.log($('.spinbox-input', this).val());
      });
    $('.spinner2').ace_spinner({
      value: 0,
      min: 0,
      max: 10000,
      step: 100,
      touch_spinner: true,
      icon_up: 'ace-icon fa fa-caret-up bigger-110',
      icon_down: 'ace-icon fa fa-caret-down bigger-110'
    });
    $('.spinner3').ace_spinner({
      value: 0,
      min: -100,
      max: 100,
      step: 10,
      on_sides: true,
      icon_up: 'ace-icon fa fa-plus bigger-110',
      icon_down: 'ace-icon fa fa-minus bigger-110',
      btn_up_class: 'btn-success',
      btn_down_class: 'btn-danger'
    });
    $('.spinner4').ace_spinner({
      value: 0,
      min: -100,
      max: 100,
      step: 10,
      on_sides: true,
      icon_up: 'ace-icon fa fa-plus',
      icon_down: 'ace-icon fa fa-minus',
      btn_up_class: 'btn-purple',
      btn_down_class: 'btn-purple'
    });
  
    //$('.spinner1').ace_spinner('disable').ace_spinner('value', 11);
    //or
    //$('.spinner1').closest('.ace-spinner').spinner('disable').spinner('enable').spinner('value', 11);//disable, enable or change value
    //$('.spinner1').closest('.ace-spinner').spinner('value', 0);//reset to 0
  
    //datepicker plugin
    //link
    $('.date-picker')
      .datepicker({
        autoclose: true,
        todayHighlight: true,
        todayBtn: true
      })
      //show datepicker when clicking on the icon
      .next().on(ace.click_event, function () {
        $(this).prev().focus();
      });
  
    //or change it into a date range picker
    $('.input-daterange').datepicker({
      autoclose: true
    });
  
    //to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
    $('input[name=date-range-picker]').daterangepicker({
      applyClass: 'btn-sm btn-success',
      cancelClass: 'btn-sm btn-default',
      locale: {
        applyLabel: 'Apply',
        cancelLabel: 'Cancel'
      }
    }).prev().on(ace.click_event, function () {
      $(this).next().focus();
    });
  
    $('.time-picker-1').timepicker({
      defaultTime: false,
      minuteStep: 1,
      showSeconds: false,
      showMeridian: false
    }).next().on(ace.click_event, function () {
      $(this).prev().focus();
    });
  
    $('.date-time-picker-1').datetimepicker().next().on(ace.click_event, function () {
      $(this).prev().focus();
    });
  
  
    $('.colorpicker1').colorpicker();
  
    $('.simple-colorpicker-1').ace_colorpicker();
  
    $(".knob").knob();
  
    $(document).one('ajaxloadstart.page', function (e) {
      $('textarea[class*=autosize]').trigger('autosize.destroy');
      $('.limiterBox,.autosizejs').remove();
      $('.daterangepicker.dropdown-menu,.colorpicker.dropdown-menu,.bootstrap-datetimepicker-widget.dropdown-menu').remove();
    });
  
    //editables
    $.fn.editable.defaults.mode = 'inline';
    $.fn.editableform.loading = "<div class='editableform-loading'><i class='ace-icon fa fa-spinner fa-spin fa-2x light-blue'></i></div>";
    $.fn.editableform.buttons = '<button type="submit" class="btn btn-info editable-submit"><i class="ace-icon fa fa-check"></i></button>' +
      '<button type="button" class="btn editable-cancel"><i class="ace-icon fa fa-times"></i></button>';
  
    $('.editable').editable();
  
    $(document).one('ajaxloadstart.page', function (e) {
      //in ajax mode, remove remaining elements before leaving page
      try {
        $('.editable').editable('destroy');
      } catch (ignore) {}
      $('[class*=select2]').remove();
    });
  
    //autonumber
    $('.autodecimal').autoNumeric('init', {
      aSep: '.',
      aDec: ',',
      mDec: 6,
      aForm: false
    });
    $('.automoney').autoNumeric('init', {
      aSep: '.',
      aDec: ',',
      mDec: 2,
      aForm: false
    });
    $('.autointeger').autoNumeric('init', {
      aSep: '.',
      aDec: ',',
      mDec: 0,
      aForm: false
    });
  
    $(document).on('click', '.checkbox_label', function (e) {
      if (e.target.type !== 'checkbox') {
        $(':checkbox', this).trigger('click');
      }
    });
  
    $(document).on('change', "input[type='checkbox']", function (e) {
      if ($(this).is(":checked")) {
        $(this).closest('tr').addClass("active");
      } else {
        $(this).closest('tr').removeClass("active");
      }
    });
  
    // tab
    $('.tabbable').stickyTabs();
  
    //sortable
    var $sortableTable = $('.sortable');
    if ($sortableTable.length > 0) {
      $sortableTable.sortable({
        handle: '.sortable-handle',
        axis: 'y',
        cursor: "move",
        update: function (event, ui) {
          var $sortable = ui.item.closest('.sortable');
          if ($sortable.data('update-url')) {
            $.post($sortable.data('update-url'), $sortable.sortable('serialize'));
          }
        }
      });
    }
    $('.sortable td').each(function () {
      $(this).css('width', $(this).width() + 'px');
    });
  
    // fix accordion inside modal
    $(document).on("click", "a.accordion-toggle", function (e) {
      $(e.target).parent().siblings('.accordion-body').on('hidden', function (e) {
        e.stopPropagation();
      });
      $(e.target).parent().siblings('.accordion-body').on('show', function (e) {
        e.stopPropagation();
      });
    });
  
    // edit color in kartu p
  
    //clor active jika inputan kosong 
    $(document).on('focus', '.special-field input', function (e) {
      $(e.target).parents('.special-field').removeClass('has-warning');
      if ($('.has-warning').length === 0) {
        $('.submit-job').removeAttr('disabled');
      }
    });
  });
  
  //show-hide hours based on selected shift
  $(document).on('change', '#start_shift_options', function (e) {
    show_hide_by_selected($, $('#start_shift_options'), $('.start_shift_hours'));
  });
  $(document).on('change', '#end_shift_options', function (e) {
    show_hide_by_selected($, $('#end_shift_options'), $('.end_shift_hours'));
  });
  