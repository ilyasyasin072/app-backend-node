var express = require('express');
var router = express.Router();
var client = require('../../helper/db_helper');
const knexClient = require('../../knex/knex.js');
const { check, validationResult } = require('express-validator');
const Jquery = require('jquery');
var jade = require('jade');



// Without Query Builder
let title_dashboard = 'Authors Views';
router.get('/mysql', function(req, res, next) {
    var authors = [];
    // GET Authors
    client.query("SELECT * FROM authors", function (err, result, fields) {
        if(err) {
            res.status(500).json({"status_code": 500, "status_massage": "internal error"})
        } else {
            for(var i=0; i < result.length; i++){
                var authorsList = {
                    'name' : result[i].name,
                    'id' : result[i].id,
                }

                authors.push(authorsList);
            }
            res.render('authors/index', {'authors': authors, title: title_dashboard });
        }
      });

    // the problem is If you using the node-mysql module, just remove the .connect and .end. Just solved the problem 
    // client.end();
});

// With Query Builder
router.get('/', async (req, res ) => {

    var obj = {
        price: 10,
        qty: 20,
    }

    let anothers = await knexClient('authors');
    try {
        res.render('authors/index', {'authors': anothers, title: title_dashboard, obj:obj });
    } catch (e) {
        res.render('authors/index', {'authors': anothers, title: title_dashboard, obj:obj });
    }
})

router.get('/create', function(req, res, next) {
    // GET CREATE Authors
    res.render('authors/create');
})

router.post('/store', 
    [
        check('name_author')
        .isLength({min : 5})
        .withMessage('Please Enter Name')
    ], async(req, res, next) => {
    //   POST Authors
    const errors = validationResult(req);
    var params = {
        name : req.body.name_author
    }


    var params = {
        name : req.body.name_author
    }
    // client.query('INSERT INTO authors SET ?', params, (err, result) => {
    // 
    // });
    knexClient.transaction(function(trx) {
        knexClient('authors').transacting(trx).insert(params)
          .then(function(resp) {
            var id = resp[0];
            // return someExternalMethod(id, trx);
          })
          .then(trx.commit)
          .catch(trx.rollback);
      })
      .then(function(resp) {
        console.log('Transaction complete.');
        res.redirect('/authors');
      })
      .catch(function(err) {
        console.error(err);
      });

    // let data = await knexClient('authors').insert(params);
    // res.redirect('/authors');

    // client.query('INSERT INTO authors SET ?', params, (err, result) => {
    //     // console.log(result);
    // });
    // let data = await knexClient('authors').insert(params);
    // res.redirect('/authors');
})

router.get('/delete/:id', async (req, res ) => {
    let anothers = await knexClient('authors').where('id', req.params.id).del();
    res.redirect('/authors');
})



// With QUery Builder INSERT

// router.post('/store' , async(req, res) => {
    
//     var params = {
//         name : req.body.name_author
//     }

//     knexClient('authors').insert(params);
//     res.redirect('/authors');
// })

router.get('/:id', function(req, res) {
    client.query("SELECT * FROM authors where id = "+ req.params.id, function(err, rows, fields) {
        var authors;
        if(err) {
            res.status(500).json({"status_code": 500, "status_massage": "internal error"})
        } else {
            if(rows.length == 1) {
                var authors1 = {
                    'name':rows[0].name,
                    'id':rows[0].id,
                }
                // renderthe authors.jade page
                res.render('authors/show', {'authors': authors1});
            } else {
                // render not found
                res.status(404).json({'status_code': 404, 'status_massage':'Not Found'})
            }
        }
    });
})

// Query Update
// con.query(
//     'UPDATE authors SET city = ? Where ID = ?',
//     ['Leipzig', 3],
//     (err, result) => {
//       if (err) throw err;
  
//       console.log(`Changed ${result.changedRows} row(s)`);
//     }
//   );

// Query Delete

// con.query(
//     'DELETE FROM authors WHERE id = ?', [5], (err, result) => {
//       if (err) throw err;
  
//       console.log(`Deleted ${result.affectedRows} row(s)`);
//     }
//   );

// Query SP
// con.query('CALL sp_get_authors()',function(err, rows){
//     if (err) throw err;
  
//     console.log('Data received from Db:');
//     console.log(rows);
//   });

// Query SP param id

// con.query('CALL sp_get_author_details(1)', (err, rows) => {
//     if(err) throw err;
  
//     console.log('Data received from Db:\n');
//     console.log(rows[0]);
//   });

// Query SP insert 

// con.query(
//     "SET @author_id = 0; CALL sp_insert_author(@author_id, 'Craig Buckler', 'Exmouth'); SELECT @author_id",
//     (err, rows) => {
//       if (err) throw err;
  
//       console.log('Data received from Db:\n');
//       console.log(rows);
//     }
//   );

module.exports = router;
