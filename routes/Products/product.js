var express = require('express');
var app = express.Router();
var client = require('../../helper/db_helper');
const knexClient = require('../../knex/knex.js');
var uniqid = require('uniqid');

// GET PRODUCT QUERY BUILDER
app.get('/', async(req, res) => {
    let product  = await knexClient('products')
                            .leftJoin('suppliers', 'suppliers.id' , '=', 'products.supplier_id')
                            .leftJoin('customers', 'customers.id', '=', 'suppliers.company_id')
                            .select('products.product_name', 'customers.customer_name', 'products.id as product_id');
    // console.log(product);
    res.render('products/index', {
        'product':product
    });
})

// GET PRODUCT CREATE QUERY BUILDER
app.get('/create', async(req, res) => {
    let supplier = await knexClient('suppliers').leftJoin('customers', 'customers.id', '=', 'suppliers.company_id').select('suppliers.id as supplier_id', 'customers.customer_name');
    res.render('products/create', {
        'supplier': supplier
    });
})

// POST PRODUCT STORE QUERY BUILDER
app.post('/store', async(req, res) => {
    let params = {
        id: uniqid('PRDT-', '-CODE'),
        product_name: req.body.product_name,
        supplier_id: req.body.supplier_id
    }
    // console.log(params);
    let product = await knexClient('products').insert(params);
    res.redirect('/products');
})

// GET PRODUCT EDIT QUERY BUILDER
// POST PRODUCT UPDATE QUERY BUILDER
// GET PRODUCT SHOW QUERY BUILDER
app.get('/show/:id', async(req, res) => {
    let product = await await knexClient('products')
    .leftJoin('suppliers', 'suppliers.id' , '=', 'products.supplier_id')
    .leftJoin('customers', 'customers.id', '=', 'suppliers.company_id')
    .where('products.id', req.params.id)
    .select('products.product_name', 'customers.customer_name', 'products.id as product_id');
    res.render('products/show', {
        'product': product[0]
    })
})
// GET PRODUCT DELETE QUERY BUILDER
app.get('/delete/:id', async(req, res) => {
    let product = await knexClient('products').where('id', req.params.id).del();
    res.json('success');
})

// JSON GET DATA
app.get('/json', async(req, res) => {
    let product  = await knexClient('products')
                        .leftJoin('suppliers', 'suppliers.id' , '=', 'products.supplier_id')
                        .leftJoin('customers', 'customers.id', '=', 'suppliers.company_id')
                        .select('products.product_name', 'customers.customer_name', 'products.id as product_id');
    let result = {'product': product}
    res.json(result);
})

module.exports = app;