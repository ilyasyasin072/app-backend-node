var express = require('express');
var app = express.Router();
var client = require('../../helper/db_helper');
const knexClient = require('../../knex/knex.js');

// GET EMPLOYEE QUERY BUILDER
app.get('/', async(req, res) => {
    let title = 'Employee';
    let employee = await knexClient('employees');
    try {
        res.render('employee/index', {'employees': employee, title: title});
    } catch (e) {
        res.render('employee/index', {'employees': employee});
    }
})

// GET EMPLOYEE CREATE FORM QUEDY BUILDER
app.get('/create', async(req, res) => {
    res.render('employee/create');
})

// POST EMPLOYEE STORE QUEDY BUILDER
app.post('/store', async(req, res) => {
    let params = {
        first_name: req.body.first_name,
        last_name:  req.body.last_name,
        address: req.body.address,
        phone: req.body.phone,
        age: req.body.age,
    }
    let employee = await knexClient('employees').insert(params);
    if(employee) {
        res.redirect('/employees');
    }
    res.redirect('/employees');
})

// GET EMPLOYEE EDIT QUEDY BUILDER
app.get('/edit/:id', async(req,res) => {
    let employee = await knexClient('employees').where('id', req.params.id).select('*');
    res.render('employee/edit', {
        'employee': employee[0]
    })
})

// POST EMPLOYEE UPDATE QUERY BUILDER
app.post('/update/:id', async(req, res) => {
    let params = {
        first_name: req.body.first_name,
        last_name:  req.body.last_name,
        address: req.body.address,
        phone: req.body.phone,
        age: req.body.age,
    }

    let employee = await knexClient('employees').where('id', req.params.id).update(params);
    res.redirect('/employees')
})

// GET EMPLOYEEE SHOW QUERY BUILDER
app.get('/show/:id', async(req, res) => {
    let employee = await knexClient('employees').where('id', req.params.id);
    console.log(employee)
    res.render('employee/show', {
        'employee': employee[0]
    })
})

// POST EMPLOYEE DELETE QUERY BUILDER

app.get('/delete/:id', async(req, res) => {
    let employee = await knexClient('employees').where('id', req.params.id).del();
    res.redirect('/employees')
})
// EXPORT FOR REOUTE ON APP.JS
module.exports = app;