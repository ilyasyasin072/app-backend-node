var express = require('express');
var router = express.Router();

/* GET home page. */
let title_dashboard = 'Dashboard';
router.get('/', function(req, res, next) {
  res.render('dashboard/index', { title: title_dashboard });
});

module.exports = router;
