CREATE TABLE employees
    ( 
        id integer auto_increment primary key,
        first_name varchar(20) not null,
        last_name varchar(20) not null,
        address varchar(100) not null,
        phone varchar(20) not null,
        age number not null)


CREATE TABLE suppiers (
    id integer auto_increment primary key,
    company_id integer not null,
)

CREATE TABLE products (
    id integer auto_increment primary key,
    product_name varchar(20) not null,
    supplier_id integer
)

CREATE TABLE orders (
    id integer auto_increment primary key,
    customer_id integer not null,
    employee_id integer not null,
    order_date datetime not null
)

CREATE TABLE customers (
    id integer auto_increment primary key,
    customer_name varchar(20) not null,
    address varchar(100) not null,
    phone varchar(20) not null
)

CREATE TABLE order_details (
    id integer auto_increment primary key,
    product_id integer not null,
    unit_price integer not null,
    qty integer not null,
    disc integer not null
)