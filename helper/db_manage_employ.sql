-- Employee
CREATE TABLE employess (
  emp_no INT AUTO_INCREMENT  NOT NULL,
  birht_date DATE NOT NULL,
  first_name VARCHAR(20) NOT NULL,
    last_name VARCHAR(20) NOT NULL,
    gender ENUM('P','L') NOT NULL,
    hire_date DATE NOT NULL,
    PRIMARY KEY(emp_no),
    INDEX (`first_name`),
    INDEX(`last_name`),
    INDEX(`gender`)
);
-- Departments
CREATE TABLE departments (
	dept_no INT AUTO_INCREMENT,
    dept_name VARCHAR(50) NOT NULL,
    PRIMARY KEY(dept_no),
    UNIQUE KEY(dept_name)
);
-- Department Employee
CREATE TABLE dept_emp(
	emp_no INT NOT NULL,
    dept_no INT NOT NULL,
    from_date DATE NOT NULL,
    to_date DATE NOT NULL, 
    KEY(emp_no),
    KEY(dept_no),
    FOREIGN KEY (emp_no) REFERENCES employess (emp_no) ON DELETE CASCADE,
    FOREIGN KEY (dept_no) REFERENCES departments (dept_no) ON DELETE CASCADE,
    PRIMARY KEY(emp_no)
);
-- 
CREATE TABLE dept_manager (
   dept_no      CHAR(4)  NOT NULL,
   emp_no       INT      NOT NULL,
   from_date    DATE     NOT NULL,
   to_date      DATE     NOT NULL,
   KEY         (emp_no),
   KEY         (dept_no),
   FOREIGN KEY (emp_no)  REFERENCES employees (emp_no)    ON DELETE CASCADE,
                                  -- ON UPDATE CASCADE??
   FOREIGN KEY (dept_no) REFERENCES departments (dept_no) ON DELETE CASCADE,
   PRIMARY KEY (emp_no, dept_no)  -- might not be unique?? Need from_date
);